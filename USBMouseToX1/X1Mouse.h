#ifndef D_INCLUDE_X1_MOUSE_H
#define D_INCLUDE_X1_MOUSE_H

class X1Mouse
{
protected:
  static bool requested_;
  int pin_msctrl_;
  int int_msctrl_;

  bool button_l_;
  bool button_r_;
  int position_x_;
  int position_y_;

public:
  X1Mouse( int pin_msctrl, int int_msctrl )
  {
    requested_ = false;
    button_l_ = false;
    button_r_ = false;
    position_x_ = 0;
    position_y_ = 0;

    pin_msctrl_ = pin_msctrl;
    int_msctrl_ = int_msctrl;
  }

  ~X1Mouse()
  {
  }

  static void interrupt_func() {
    requested_ = true;
  }
  
  void  start()
  {
    Serial.begin(4800);

    pinMode( pin_msctrl_, INPUT_PULLUP );
    
    attachInterrupt( int_msctrl_, interrupt_func, FALLING );
  }

  void update()
  {
    if( requested_ == true )
    {
      requested_ = false;

      unsigned char status;
      char x;
      char y;
      if( button_l_ == true ){
        status |= 0x01;
      }
      if( button_r_ == true ){
        status |= 0x02;
      }

      if( position_x_ > 127 ){
        status |= 0x10;
        position_x_ = 127;
      }
      if( position_x_ < -128 ){
        status |= 0x20;
        position_x_ = -128;
      }
      if( position_y_ > 127 ){
        status |= 0x40;
        position_y_ = 127;
      }
      if( position_y_ < -128 ){
        status |= 0x80;
        position_y_ = -128;
      }
      
      x = position_x_;
      y = position_y_;
      position_x_ = 0;
      position_y_ = 0;
      
      Serial.write( status );
      Serial.write( x );
      Serial.write( y );
    }
  }

  void set( bool button_l, bool button_r, int position_x, int position_y )
  {
    button_l_ = button_l;
    button_r_ = button_r;
    position_x_ += position_x;
    position_y_ += position_y;
  }

  void setButtonL( bool button_l )
  {
    button_l_ = button_l;
  }
  void setButtonR( bool button_r )
  {
    button_r_ = button_r;
  }
  void setPosition( int position_x, int position_y )
  {
    position_x_ += position_x;
    position_y_ += position_y;
  }
};

static bool X1Mouse::requested_ = false;


#endif
