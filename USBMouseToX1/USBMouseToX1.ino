#include <hidboot.h>
#include <usbhub.h>

// Satisfy the IDE, which needs to see the include statment in the ino too.
#ifdef dobogusinclude
#include <spi4teensy3.h>
#include <SPI.h>
#endif

#include "IntervalCheck.h"
#include "X1Mouse.h"

// LED表示定義
IntervalCheck led_ctrl_( 100 );
unsigned int led_pattern_ = 0;
#define D_LED_PIN (3)

// マウスIF定義
#define D_X1_MOUSE_PIN (2)
#define D_X1_MOUSE_INT (0)
X1Mouse x1_mouse_( D_X1_MOUSE_PIN, D_X1_MOUSE_INT );

class MouseRptParser : public MouseReportParser
{
protected:
	void OnMouseMove	(MOUSEINFO *mi);
	void OnLeftButtonUp	(MOUSEINFO *mi);
	void OnLeftButtonDown	(MOUSEINFO *mi);
	void OnRightButtonUp	(MOUSEINFO *mi);
	void OnRightButtonDown	(MOUSEINFO *mi);
	void OnMiddleButtonUp	(MOUSEINFO *mi);
	void OnMiddleButtonDown	(MOUSEINFO *mi);
};
void MouseRptParser::OnMouseMove(MOUSEINFO *mi)
{
//  Serial.print("dx=");
//  Serial.print(mi->dX, DEC);
//  Serial.print(" dy=");
//  Serial.println(mi->dY, DEC);
  led_pattern_ = 0x0003;
  x1_mouse_.setPosition( mi->dX, mi->dY );
};
void MouseRptParser::OnLeftButtonUp	(MOUSEINFO *mi)
{
//  Serial.println("L Butt Up");
  x1_mouse_.setButtonL( false );
};
void MouseRptParser::OnLeftButtonDown	(MOUSEINFO *mi)
{
//  Serial.println("L Butt Dn");
  led_pattern_ = 0x0003;
  x1_mouse_.setButtonL( true );
};
void MouseRptParser::OnRightButtonUp	(MOUSEINFO *mi)
{
//  Serial.println("R Butt Up");
  x1_mouse_.setButtonR( false );
};
void MouseRptParser::OnRightButtonDown	(MOUSEINFO *mi)
{
//  Serial.println("R Butt Dn");
  led_pattern_ = 0x0003;
  x1_mouse_.setButtonR( true );
};
void MouseRptParser::OnMiddleButtonUp	(MOUSEINFO *mi)
{
//  Serial.println("M Butt Up");
};
void MouseRptParser::OnMiddleButtonDown	(MOUSEINFO *mi)
{
//  Serial.println("M Butt Dn");
  led_pattern_ = 0x0003;
};

USB     Usb;
USBHub     Hub(&Usb);
HIDBoot<USB_HID_PROTOCOL_MOUSE>    HidMouse(&Usb);

MouseRptParser                               Prs;

void setup()
{
  pinMode( D_LED_PIN, OUTPUT );

//  Serial.begin( 115200 );
#if !defined(__MIPSEL__)
  while (!Serial); // Wait for serial port to connect - used on Leonardo, Teensy and other boards with built-in USB CDC serial connection
#endif
//  Serial.println("Start");

  delay( 2000 );

  // USBIF初期化
  if (Usb.Init() == -1)
  {
//  Serial.println("OSC did not start.");
    led_pattern_ = 0x3333;
  }

  // HIDマウスパーサ定義
  HidMouse.SetReportParser( 0, &Prs );

  // マウスIF起動
  x1_mouse_.start();
}


void loop()
{
  // USBIF状態更新
  Usb.Task();

  // マウスIF状態更新
  x1_mouse_.update();

  // LED表示更新
  if( led_ctrl_.check() == true )
  {
    if( (led_pattern_ & 0x0001) == 0 )
    {
      digitalWrite( D_LED_PIN, LOW );
    }
    else
    {
      digitalWrite( D_LED_PIN, HIGH );
    }
    led_pattern_ >>= 1;
  }
}

