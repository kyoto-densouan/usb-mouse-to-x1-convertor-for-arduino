# README #

USB HID Mouse controler to SHARP X1/X68000 Mouse port Convertor for Arduino pro mini/UNO  

##Use Library##

USB Host Shield Library  

##Wiring##

Arduino  

   pinTX  - X1 Mouse port(7pin mini DIN)-pin3:MSCTRL  
   pin2   - X1 Mouse port(7pin mini DIN)-pin4:MSDATA  
   pin3   - Mouse active LED(A)  
   pin9   - USB Host Shield  
   pin10  - USB Host Shield  
   pin11  - USB Host Shield  
   pin12  - USB Host Shield  
   pin13  - USB Host Shield  
   pinRST - USB Host Shield  
   pinVCC - X1 Mouse port(7pin mini DIN)-pin1:VCC  
   pinGND - X1 Mouse port(7pin mini DIN)-pin5:GND  
   pinGND - USB Host Shield  
   pin3.3 - USB Host Shield  

##Other##

https://www.youtube.com/watch?v=nDJJpHSNovk  
